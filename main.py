from neomodel import StringProperty, StructuredNode, config, db


class TestNode(StructuredNode):
    __label__ = 'Test'

    name = StringProperty(required=True)


def run():
    config.DATABASE_URL = "neo4j://neo4j:password@neo4j:7687"
    # query = "MATCH (n) RETURN COUNT(n)"

    try:
        node = TestNode(name="Test")
        node.save()

        print("Node saved !")

        test_nodes = TestNode.nodes.all()
        print(len(test_nodes))

    except Exception as e:
        print(e)


if __name__ == "__main__":
    run()
